package io.github.hidroh.materialistic;

/**
 * Created by caev0 on 26/11/2017.
 */

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

public final class ColorMatcher
{
    private ColorMatcher() {
        // do not instantiate
    }

    /**
     * Returns a matcher that matches {@link android.widget.TextView}s based on color value.
     *
     * @param color {@link String} with text to match
     */
    @NonNull
    public static Matcher<View> withTextColor(final int color) {
        return withTextColor(Matchers.comparesEqualTo(color));
    }

    /**
     * Returns a matcher that matches {@link android.widget.TextView}s based on color property value.
     *
     * @param stringMatcher {@link Matcher} of {@link int} with text to match
     */
    @NonNull
    public static Matcher<View> withTextColor(final Matcher<Integer> stringMatcher) {

        return new BoundedMatcher<View, Button>(Button.class) {

            @Override
            public void describeTo(final Description description) {
                description.appendText("with text color: ");
                stringMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(final Button textView) {
                return stringMatcher.matches(textView.getCurrentTextColor());
            }
        };
    }

    /**
     * Returns a matcher that matches a descendant of {@link android.widget.TextView} that is displaying the error
     * string associated with the given resource id.
     *
     * @param resourceId the string resource the text view is expected to hold.
     */
//    @NonNull
//    public static Matcher<View> withTextColor(@StringRes final int resourceId) {
//
//        return new BoundedMatcher<View, TextView>(TextView.class) {
//            private String resourceName = null;
//            private String expectedText = null;
//
//            @Override
//            public void describeTo(final Description description) {
//                description.appendText("with error text from resource id: ");
//                description.appendValue(resourceId);
//                if (null != resourceName) {
//                    description.appendText("[");
//                    description.appendText(resourceName);
//                    description.appendText("]");
//                }
//                if (null != expectedText) {
//                    description.appendText(" value: ");
//                    description.appendText(expectedText);
//                }
//            }
//
//            @Override
//            public boolean matchesSafely(final TextView textView) {
//                if (null == expectedText) {
//                    try {
//                        expectedText = textView.getResources().getString(resourceId);
//                        resourceName = textView.getResources().getResourceEntryName(resourceId);
//                    } catch (Resources.NotFoundException ignored) {
//                        // view could be from a context unaware of the resource id
//                    }
//                }
//                return null != expectedText && expectedText.equals(textView.getError());
//            }
//        };
//    }
}
